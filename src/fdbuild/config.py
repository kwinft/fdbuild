# SPDX-FileCopyrightText: 2018 Roman Gilg <subdiff@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# deduced at start
WORK_PATH = ""
TOPLVL_PATH = ""

# Command line arguments object
args = None

# for project settings files
SETTGS_NAME = "fdbuild.yaml"

# Global value being determined in count phase before work.
PROJECTS_COUNT = 0
