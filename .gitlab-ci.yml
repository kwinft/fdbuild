# SPDX-FileCopyrightText: 2022 Roman Gilg <subdiff@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

stages:
  - Compliance
  - Analysis
  - Template Builds

workflow:
  rules:
    - when: always

include:
  - project: kwinft/tooling
    ref: master
    file:
      - '/docs/gitlab-ci/commits.yml'


####################################################################################################
#
# Compliance
#

Message Lint:
  extends: .message-lint
  stage: Compliance

Copyright Lint:
  stage: Compliance
  image: python
  before_script:
    - pip install tox
  script:
    - tox -e reuse

Code Style:
  stage: Compliance
  image: python
  before_script:
    - pip install tox
  script:
    - tox -e black

Templates Style:
  stage: Compliance
  image: python
  before_script:
    - pip install tox
  script:
    - tox -e yamllint

####################################################################################################
#
# Analysis
#

.common-tests:
  stage: Analysis
  before_script:
    - pip install tox
  script:
    - tox -e sample-plugin,templates

Old Python Tests:
  extends: .common-tests
  image: python:3.8

Tests:
  extends: .common-tests
  image: python


####################################################################################################
#
# Template Builds
#

.template-builds-common:
  stage: Template Builds
  tags:
    - image-build
  when: manual
  image: archlinux
  before_script:
    - pacman -Syu --quiet --noconfirm
    - pacman -S --needed --quiet --noconfirm python-pip git
    - python -m venv /opt/venv
    - source /opt/venv/bin/activate
    - pip install .
    - INSTALL_DIR=$(pwd)/ci-install
    - mkdir ci-build
    - cd ci-build
    - PATH=$INSTALL_DIR/bin:$PATH

.template-builds-graphics:
  extends: .template-builds-common
  before_script:
    - !reference [.template-builds-common, before_script]
    - pip install mako ply
    # Below all dependencies starting from avisynthplus are for ffmpeg
    - pacman -S --needed --quiet --noconfirm
        base-devel clang cmake llvm meson ninja pkgconf
        rust rust-bindgen libclc spirv-llvm-translator
        check glslang gperf libevdev libfontenc
        libunwind libwacom libxslt mtdev xmlto
        docbook-xsl doxygen graphviz perl-uri pcre yasm
        avisynthplus frei0r-plugins ladspa libiec61883 libass libbluray libbs2b gsm libjxl
        libmodplug opencore-amr libpulse snappy libsoxr libssh speex srt libtheora v4l-utils
        vid.stab vmaf libvpx x264 xvidcore zimg jack


linux-graphics:
  extends: .template-builds-graphics
  before_script:
    - !reference [.template-builds-graphics, before_script]
  script:
    - fdbuild --init-with-template linux-graphics-meta
    - 'sed -i "s.source:.source:\n  depth: 1.g" linux-graphics-meta/fdbuild.yaml'
    - 'sed -i "s.  path: /usr/local.  path: $INSTALL_DIR.g" linux-graphics-meta/fdbuild.yaml'
    - fdbuild -w linux-graphics-meta
  artifacts:
    paths:
      - ci-install

linux-qt6:
  extends: .template-builds-graphics
  when: on_success
  needs:
    - job: linux-graphics
      artifacts: true
  before_script:
    - !reference [.template-builds-graphics, before_script]
  script:
    - fdbuild --init-with-template linux-desktop-meta
    - 'sed -i "s.source:.source:\n  depth: 1.g" linux-desktop-meta/fdbuild.yaml'
    - 'sed -i "s.  path: /usr/local.  path: $INSTALL_DIR.g" linux-desktop-meta/fdbuild.yaml'
    - fdbuild -w linux-desktop-meta/qt6
  artifacts:
    paths:
      - ci-install

linux-desktop:
  extends: .template-builds-graphics
  when: on_success
  needs:
    - job: linux-qt6
      artifacts: true
  before_script:
    - !reference [linux-qt6, before_script]
    - pip install pycairo chai libevdev pytest attr pygobject
    - pacman -S --needed --quiet --noconfirm
        boost exiv2 gobject-introspection gsettings-desktop-schemas gtk-doc itstool
        libcanberra libnm libpulse libstemmer libqalculate libyaml lmdb lm_sensors
        modemmanager polkit sassc qrencode zxing-cpp gi-docgen xdotool microsoft-gsl
  script:
    - fdbuild --init-with-template winft-plasma-meta
    - 'sed -i "s.source:.source:\n  depth: 1.g" winft-plasma-meta/fdbuild.yaml'
    - 'sed -i "s.  path: /usr/local.  path: $INSTALL_DIR.g" winft-plasma-meta/fdbuild.yaml'
    # Disable Qt5 builds for Phonon and plasma-integration
    - 'sed -i "s.  options:.  options:\n  - PHONON_BUILD_QT5=OFF\n  - BUILD_QT5=OFF.g" winft-plasma-meta/kde/fdbuild.yaml'
    # breeze-grub comes without CMake file
    # TODO(romangg): need to adapt structurizer, so it adds a hook instead
    - "sed -i '/- breeze-grub/d' winft-plasma-meta/kde/fdbuild.yaml"
    # plymouth is in AUR
    - "sed -i '/- breeze-plymouth/d' winft-plasma-meta/kde/fdbuild.yaml"
    - "sed -i '/- plymouth-kcm/d' winft-plasma-meta/kde/fdbuild.yaml"
    # Does not yet build with Qt6
    - "sed -i '/- kdeconnect-kde/d' winft-plasma-meta/kde/fdbuild.yaml"
    - "sed -i '/- plasma-bigscreen/d' winft-plasma-meta/kde/fdbuild.yaml"
    # Requires Qt6WebEngineQuick, what we don't build.
    - "sed -i '/- aura-browser/d' winft-plasma-meta/kde/fdbuild.yaml"
    # Pulls in flatpak
    - "sed -i '/- flatpak-kcm/d' winft-plasma-meta/kde/fdbuild.yaml"
    # Pulls in all of GTK3.
    - "sed -i '/- kde-gtk-config/d' winft-plasma-meta/kde/fdbuild.yaml"
    # Pulls in kdsoap-qt6 that pulls in Qt6.
    - "sed -i '/- kdsoap-ws-discovery-client/d' winft-plasma-meta/kde/fdbuild.yaml"
    # Pulls in xf86-input-wacom that pulls in libX
    - "sed -i '/- wacomtablet/d' winft-plasma-meta/kde/fdbuild.yaml"
    - fdbuild -w winft-plasma-meta
